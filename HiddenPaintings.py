# -*- coding: UTF-8 -*-

# use matplotlib
import matplotlib.pyplot as plt
import numpy as np

x, y = [], []
file = open("secret_map.txt", "r")
line = file.readline()
while line:
    strs = line.split()
    y.append(int(strs[0], 16))
    x.append(int(strs[1], 16))
    line = file.readline()


# Note that even in the OO-style, we use `.pyplot.figure` to create the figure.
fig, ax = plt.subplots()  # Create a figure and an axes.
ax.scatter(x, y)  # Plot some data on the axes.
ax.set_xlabel('x label')  # Add an x-label to the axes.
ax.set_ylabel('y label')  # Add a y-label to the axes.
ax.set_title("Simple Plot")  # Add a title to the axes.
ax.legend()  # Add a legend.
# plt.gca().invert_xaxis()
plt.gca().invert_yaxis()
plt.show()  # to make the plot appear
