# -*- coding: UTF-8 -*-

from pwn import *  # pylint: disable=unused-wildcard-import


def attack(ip, port):
    number = "0"
    while True:
        r = remote(ip, port)
        r.recvline()  # Can you guess the number to win the flag lottery?
        r.sendline(number)
        s = r.recvline()  # Nope! The winning number was 0.400271794791, better luck next time!
        print(s)
        strs = s.split()
        if not strs or strs[0] != b'Nope!':
            print(r.recvline())
            break
        number = strs[5][0:-1]  # trim ,


if __name__ == "__main__":
    attack('ff5363160dc0c8d5.247ctf.com', 50374)
