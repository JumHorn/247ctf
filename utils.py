# -*- coding: UTF-8 -*-

# specific functions for 247ctf

def addressParse(address):
    strs = address.split(':')
    if len(strs) < 3:
        return ("", 0)
    ip = strs[1][2:]
    port = int(strs[2])
    return (ip, port)


def bytesToHexString(content, space_seperated=True):
    if space_seperated:
        return "".join(["%02X " % b for b in content])
    else:
        return "".join(["%02X" % b for b in content])


def readFromFile(file):
    with open(file, 'rb') as f:
        return f.read()


def writeToFile(content, file):
    with open(file, 'wb') as f:
        f.write(content)
