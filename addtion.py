# -*- coding: UTF-8 -*-

from socket import *  # pylint: disable=unused-wildcard-import


def create(ip, port):
    sk = socket()
    sk.connect((ip, port))
    return sk


def parseAddition(content):
    strlist = content.split("\r\n")
    for line in strlist:
        if line.startswith("What is the answer to"):
            lines = line.split(" ")
            return str(int(lines[-1][0:-1]) + int(lines[-3]))
    return ""


if __name__ == "__main__":
    sk = create("d92e350ba4d305c6.247ctf.com", 50027)
    while True:
        content = sk.recv(65536)
        print(content)
        res = parseAddition(content.decode('utf-8'))
        if not res:
            break
        sk.send((res + "\r\n").encode('utf-8'))
