# -*- coding: UTF-8 -*-

from pwn import *  # pylint: disable=unused-wildcard-import
import utils  # miscellaneous for 247ctf


def attack(ip, port):
    r = remote(ip, port)
    s = r.recvline()
    print(s)


if __name__ == "__main__":
    address = utils.addressParse("tcp://112e31ca5214c9b4.247ctf.com:50050")
    attack(*address)
