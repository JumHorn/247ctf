# -*- coding: UTF-8 -*-

from itertools import cycle
import utils  # miscellaneous for 247ctf


def convert(key, filename):
    with open(filename, "rb") as f:
        content = f.read()

    converted_content = bytes([(a ^ b)
                               for a, b in zip(content, cycle(key))])
    with open(filename + utils.bytesToHexString(key, False) + ".jpg", "wb") as f:
        f.write(converted_content)


def getkey(filename, signatures):
    with open(filename, "rb") as f:
        content = f.read()
    # for i in range(0, 10):
    #     print("%x" % (content[i])),
    keys = []
    for sig in signatures:
        key = bytes.fromhex("".join(sig.split()))
        keys.append(bytes([a ^ b
                           for a, b in zip(key, content)]))
        # for a, b in zip(key, content):
        #     print("%x %x %x" % (a, b, a ^ b))
    return keys


if __name__ == "__main__":
    filename = "my_magic_bytes.jpg.enc"
    signatures = ["FF D8 FF DB", "FF D8 FF EE",
                  "FF D8 FF E0 00 10 4A 46 49 46 00 01"]
    keys = getkey(filename, signatures)
    for key in keys:
        convert(key, filename)
