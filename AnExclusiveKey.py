# -*- coding: UTF-8 -*-

# pow(16,32)数据量太大，只能随机猜测，然后验证猜测结果正确性
# 猜测结果正确性和保存的代码未完成

import utils  # miscellaneous for 247ctf
import sys
from itertools import cycle


def processKey(key):
    """
    key
    247CTF{32-hex}
    len of key is 40
    """
    len_of_key = 40
    if len(key) < len_of_key:
        key += "#" * (len_of_key - len(key))
    if len(key) > len_of_key:
        key = key[0:len_of_key]
    return key.encode()


def decodeFile(input, key, output):
    content = utils.readFromFile(input)
    decode_content = bytes([a ^ b for a, b in zip(content, cycle(key))])
    utils.writeToFile(decode_content, output)


if __name__ == "__main__":
    # key = sys.argv[1]  # get input key from cmd
    # input_file = sys.argv[2]  # get input file from cmd

    # for debug
    key = "247CTF{"
    input_file = "exclusive_key"

    output_file = "decode.html"
    key = processKey(key)

    decodeFile(input_file, key, output_file)
